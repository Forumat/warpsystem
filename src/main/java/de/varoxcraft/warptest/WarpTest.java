package de.varoxcraft.warptest;

import de.varoxcraft.warptest.commands.SetWarpCommand;
import de.varoxcraft.warptest.commands.WarpCommand;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;

public final class WarpTest extends JavaPlugin {

    private String prefix;
    private static WarpTest instance;
    private File file;
    private YamlConfiguration yamlConfiguration;

    @Override
    public void onEnable() {
        prefix = "§8[§b§lVAROX§8] ";
        instance = this;
        System.out.println("Plugin wird enabled!");
        file = new File(getDataFolder(), "warps.yml");
        yamlConfiguration = YamlConfiguration.loadConfiguration(file);
        try {
            yamlConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WarpObject.loadWarps();

        getCommand("setwarp").setExecutor(new SetWarpCommand());
        getCommand("warp").setExecutor(new WarpCommand());
    }

    @Override
    public void onDisable() {
        System.out.println("Plugin wird disabled!");
    }


    public YamlConfiguration getYamlConfiguration() {
        return yamlConfiguration;
    }
    public File getFile() {
        return file;
    }
    public static WarpTest getInstance() {
        return instance;
    }
    public String getPrefix() {
        return prefix;
    }
}
