package de.varoxcraft.warptest;

import org.bukkit.Location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WarpObject {

    private String name;
    private Location location;
    private static List<WarpObject> warps = new ArrayList<>();

    public WarpObject(String name, Location location) {
        this.name = name;
        this.location = location;
        warps.add(this);
    }

    public static void loadWarps(){
        for(String s : WarpTest.getInstance().getYamlConfiguration().getConfigurationSection("Warps").getValues(false).keySet()){
            new WarpObject(s, (Location) WarpTest.getInstance().getYamlConfiguration().get("Warps." + s));
        }
    }

    public void save() throws IOException {
        WarpTest.getInstance().getYamlConfiguration().set("Warp." + getName(), getLocation());
        WarpTest.getInstance().getYamlConfiguration().save(
                WarpTest.getInstance().getFile()
        );
    }

    public static WarpObject getWarpByName(String name){
        WarpObject returnValue = null;
        for(WarpObject warpObject : warps){
            if(warpObject.getName().equalsIgnoreCase(name)){
                returnValue = warpObject;
            }
        }
        return returnValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public static List<WarpObject> getWarps() {
        return warps;
    }
}
