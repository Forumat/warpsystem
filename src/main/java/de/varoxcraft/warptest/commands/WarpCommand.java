package de.varoxcraft.warptest.commands;

import de.varoxcraft.warptest.WarpObject;
import de.varoxcraft.warptest.WarpTest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            if(args.length != 1) return false;
            WarpObject warpObject = WarpObject.getWarpByName(args[0]);
            if(warpObject == null){
                sender.sendMessage(WarpTest.getInstance().getPrefix() + "§cDieser Warp existiert nicht! Folgende Warps sind registriert:");
                for (WarpObject warpObject1 : WarpObject.getWarps()) {
                    sender.sendMessage(WarpTest.getInstance().getPrefix() + "§8- §e" + warpObject1.getName());
                }
                return false;
            }
            ((Player) sender).teleport(warpObject.getLocation());
            sender.sendMessage(WarpTest.getInstance().getPrefix() + "§aDu wurdest zum Warp-Punkt §e" + warpObject.getName() + "§a teleportiert!");
        }

        return false;
    }
}
