package de.varoxcraft.warptest.commands;

import de.varoxcraft.warptest.WarpObject;
import de.varoxcraft.warptest.WarpTest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

public class SetWarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player) {
            if (sender.hasPermission("warps.addwarp")) {
                if(args.length == 1){
                    String warpName = args[0];
                    try {
                        new WarpObject(warpName, ((Player) sender).getLocation()).save();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    sender.sendMessage(WarpTest.getInstance().getPrefix() + "§aDu hast einen neuen Warp erstellt!");
                }else{
                    sender.sendMessage(WarpTest.getInstance().getPrefix() + "§cNutze: /setwarp <Name>!");
                }
            }else{
                sender.sendMessage(WarpTest.getInstance().getPrefix() + "§cDazu hast du keine Rechte!");
            }
        }
        return false;
    }
}
